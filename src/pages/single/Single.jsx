import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Sidebar from "../../components/sidebar/Sidebar";
import SinglePost from "../../components/singlePost/SinglePost";
import "./single.css";

export default function Single() {
  const { _id } = useParams();
  const [post, setPost] = useState({});

  useEffect(() => {
    const fetchPost = async () => {
      const res = await axios.get(`/article/${_id}`)
      const { data } = res.data
      setPost(data)
    }

    fetchPost()
  })

  return (
    <div className="single">
      <SinglePost post={post} />
      <Sidebar />
    </div>
  );
}
