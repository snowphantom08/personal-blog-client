import axios from "axios";
import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import Header from "../../components/header/Header";
import Posts from "../../components/posts/Posts";
import Sidebar from "../../components/sidebar/Sidebar";

export default function Search() {
  const {search} = useLocation();
  const [posts, setPosts] = useState([]);

  useEffect(() => {
      const fetchPosts = async () => {
          const res = await axios.get("/article" + search);
          const { data } = res.data
          setPosts(data);
      }
      fetchPosts();
  }, [search])
  return (
      <>
          <div className="home">
              <Posts posts={posts}/>
              <Sidebar/>
          </div>
      </>
  );
}