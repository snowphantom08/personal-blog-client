export const toHumanDate = (data) => {
  if (!data) return data

  var datetime = new Date(Date.parse(data))
  return datetime.toDateString()
}