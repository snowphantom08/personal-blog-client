import { Link } from "react-router-dom";
import "./post.css";

export default function Post({ post }) {
  return (
    <div className="post">
      {/*{post.photo && <img className="postImg" src={PF + post.photo} alt="" />}*/}
      <img className="postImg" src="http://www.elle.vn/wp-content/uploads/2017/07/25/hinh-anh-dep-1.jpg" alt=""/>
      <div className="postInfo">
        {/*<div className="postCats">*/}
        {/*  {post.categories.map((c) => (*/}
        {/*    <span className="postCat">{c.name}</span>*/}
        {/*  ))}*/}
        {/*</div>*/}
        <div className="postCats">
          <span className="postCat">{post.category?.title}</span>
        </div>
        <Link to={`/post/${post._id}`} className="link">
          <span className="postTitle">{post.title}</span>
        </Link>
        <hr/>
        <span className="postDate">
          {new Date(post.createdAt).toDateString()}
        </span>
      </div>
      <p className="postDesc">{post.description}</p>
    </div>
  );
}
