import { Link } from "react-router-dom";
import { toHumanDate } from "../../utils/dateTime";
import "./singlePost.css";

export default function SinglePost({ post }) {
  return (
    <div className="singlePost">
      <div className="singlePostWrapper">
        <img
          className="singlePostImg"
          src="https://images.pexels.com/photos/6685428/pexels-photo-6685428.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
          alt=""
        />
        <h1 className="singlePostTitle">
          { post.title }
          <div className="singlePostEdit">
            <i className="singlePostIcon far fa-edit"></i>
            <i className="singlePostIcon far fa-trash-alt"></i>
          </div>
        </h1>
        <div className="singlePostInfo">
          <span>{ toHumanDate(post.createdAt) }</span>
        </div>
        <p className="singlePostDesc">
          { post.description }
          <br />
          <br />
          { post.content }
        </p>
      </div>
    </div>
  );
}
