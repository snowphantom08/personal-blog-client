import { Link } from "react-router-dom";
import './categoryItem.css'

export default function CategoryItem({ category }) {
  return (
    <div class="category">
      <Link className="link" to={`/posts?category=${category._id}`}>
        { category.name }
      </Link>
    </div>
  )
}