import Post from "../post/Post";
import "./posts.css";

export default function Posts({ posts }) {
  return (
    <div className="posts">
      {
        (posts || []).map((p) => (
          <Post post={p} />
        ))
      }
      {
        (!posts || posts.length < 1) &&
        <div className="noposts">
          No posts found!
        </div>
      }
    </div>
  );
}
